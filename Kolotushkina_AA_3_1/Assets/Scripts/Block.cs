﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.iOS;

namespace Arkanoid
{
    public class Block : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        { 
            GameManager.Instance.RemoveBlock(this);
        }
    }
}