﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Arkanoid
{
    public class GameManager : MonoBehaviour
    {
        [Header("Players")]
        [SerializeField] private FirstPlayerController _firstPlayer;
        [SerializeField] private PlayerController _secondPlayer;
        [SerializeField] private List<HealthTracker> _healthTrackers;
        
        [SerializeField, Tooltip("Кол-во жизней игроков на уровень")]
        private int _maxHealth;
        private int _health;
        
        [SerializeField, Tooltip("Коэффициент скорости игрока. Умножается на единичный вектор направления движения")]
        private float _speedMultiplier;
        public float GetSpeedMultiplier => _speedMultiplier;

        [Header("Blocks")]
        [SerializeField, Tooltip("Объект, хранящий копии блоков в сцене")]
        private Transform _holder;
        [SerializeField, Tooltip("Точка, обозначающая минимальную позицию зоны спавна блоков")]
        private Vector3 _minSpawnPosition;
        [SerializeField, Tooltip("Точка, обозначающая максимальную позицию зоны спавна блоков")]
        private Vector3 _maxSpawnPosition;
        [SerializeField, Tooltip("Префаб блока")]
        private Block _blockPrefab;
        [SerializeField, Tooltip("Кол-во блоков, генерируемое на уровне")]
        private int _blocksAmount;
        
        private List<Block> _blocks = new List<Block>();

        [Header("Ball")]
        [SerializeField, Tooltip("Префаб шара")]
        private BallController _ball;
        [SerializeField, Tooltip("Кол-во столкновений, после которых скорость шара будет максимальной")]
        private int _collisionsToMaxSpeed;
        [SerializeField, Tooltip("Начальная скорость перемещения шара")]
        private float _startBallSpeed;
        [SerializeField, Tooltip("Максимально возможная скорость перемещения шара")]
        private float _maxBallSpeed;
        
        private int _collisionCounter;
        private float _currentSpeed;

        [Header("Rooms")]
        [SerializeField] private List<Transform> _rooms;
        [SerializeField, Tooltip("Время перемещения к следующему уровню")]
        private float _changeLevelTime;

        [HideInInspector] public bool InputFreezed;
        
        public BallController GetBall => _ball;
        public float GetBallSpeed => _currentSpeed;
        
        public static GameManager Instance;

        private void Awake()
        {
            if (Instance != null)
                Destroy(this);
            else
                Instance = this;
        }

        private void Start()
        {
            GenerateBlocks();
            ResetBallParams();
            
            _health = _maxHealth;
            foreach (var hpTracker in _healthTrackers)
                hpTracker.UpdateHealth(_health);
            
            InputFreezed = false;
        }
        
        private void GenerateBlocks() // Создание блоков
        {
            for (int i = 0; i < _blocksAmount; i++)
            {
                // Генерация случайной позиции и поворота
                var rndX = Random.Range(_minSpawnPosition.x, _maxSpawnPosition.x);
                var rndY = Random.Range(_minSpawnPosition.y, _maxSpawnPosition.y);
                var rndZ = Random.Range(_minSpawnPosition.z, _maxSpawnPosition.z);
                var rndRotation = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
                
                var block = Instantiate(_blockPrefab, new Vector3(rndX, rndY, rndZ), Quaternion.Euler(rndRotation), _holder);
                _blocks.Add(block);
            }
        }
        
        public void RemoveBlock(Block block) 
        {
            _blocks.Remove(block);
            Destroy(block.gameObject);
            
            if (_blocks.Count < 1)
            {
                ActivateWinState();
            }
            
            IncreaseCollisionCounter();
        }
        
        private void IncreaseCollisionCounter() // Пересчет скорости шара по кол-ву столкновений
        {
            _collisionCounter++;
            float percentage = Mathf.Clamp01((float)_collisionCounter / _collisionsToMaxSpeed);
            _currentSpeed = Mathf.Lerp(_startBallSpeed, _maxBallSpeed, percentage);
        }
        
        public void DeathZoneCollision() // Обработка ситуации вылета шара за пределы игрового поля
        {
            _health--;
            foreach (var hpTracker in _healthTrackers)
                hpTracker.UpdateHealth(_health);
            
            if (_health < 1)
            {
                ActivateLoseState();
            }
            else
            {
                Debug.Log("Health left: " + _health);
                ResetBallParams();
            }
        }

        private void ActivateWinState()
        {
            Debug.Log("This is win!");
            StartCoroutine(ChangeLevel());
        }

        private void ActivateLoseState()
        {
            Debug.Log("This is lose!");
        }

        private void ResetBallParams() // Привязка шара к первому игроку и сброс скорости
        {
            _firstPlayer.SetBall();
            _currentSpeed = _startBallSpeed;
            _collisionCounter = 0;
        }

        private IEnumerator ChangeLevel() // Анимация смены уровня
        {
            InputFreezed = true;
            _ball.HideBall();

            var secondRoomStartPos = _rooms[1].position;
            var timer = 0f;
            var moveStep = secondRoomStartPos.x / _changeLevelTime;

            while (timer < _changeLevelTime)
            {
                foreach (var room in _rooms)
                {
                    room.position -= moveStep * room.right * Time.deltaTime;
                }
                timer += Time.deltaTime;
                yield return null;
            }

            _rooms[0].position = secondRoomStartPos;
            _rooms[1].position = Vector3.zero;

            var roomToReplace = _rooms[0];
            _rooms[0] = _rooms[1];
            _rooms[1] = roomToReplace;

            yield return new WaitForSeconds(1f);
            
            _health = _maxHealth;
            GenerateBlocks();
            ResetBallParams();
            _ball.ShowBall();
            InputFreezed = false;
        }
    }
}

