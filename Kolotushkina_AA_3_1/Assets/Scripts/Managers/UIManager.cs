﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Arkanoid
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;
        [SerializeField] private List<Canvas> _playerCanvas;
        [SerializeField] private GameObject _pauseMenu; // Префаб меню паузы

        private bool _gameOnPause;

        private void Awake()
        {
            if (Instance != null)
                Destroy(this);
            else
                Instance = this;
        }

        public void Pause() // Вызов панели паузы на оба экрана
        {
            if (_gameOnPause) return;
            Time.timeScale = 0f;
            
            foreach (var c in _playerCanvas)
                Instantiate(_pauseMenu, c.transform);
        }

        public void CloseAllPauseMenu() // Закрытие всех панелей с паузой
        {
            foreach (var canvas in _playerCanvas)
            {
                canvas.GetComponentInChildren<PauseMenu>().Deactivate();
            }
        }
    }
}
