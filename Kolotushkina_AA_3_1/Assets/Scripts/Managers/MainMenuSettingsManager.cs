﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Arkanoid
{
    public class MainMenuSettingsManager : MonoBehaviour, ISettingsManager
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private TextMeshProUGUI _versionText; // Текстовое поле с версией
        [SerializeField] private RectTransform _title;
        [SerializeField] private List<RectTransform> _buttons;
        [SerializeField] private Settings _settingsPanel;
        
        [Header("Animation Settings")]
        [SerializeField] private float _animationTimer;
        private float _currentTimer;
        [SerializeField] private float _titleYPos; // Позиция, на которую должно переместиться название в конце анимации
        // Стартовые позиции элементов
        private float _titleStartYPos;
        private float _buttonStartXPos;

        private bool _coroutineIsOn;

        private void OnEnable()
        {
            _titleStartYPos = _title.anchoredPosition.y;
            _buttonStartXPos = _buttons[1].anchoredPosition.x;
            
            StartCoroutine(OpenMenu(true));
        }

        private void Start()
        {
            _versionText.text = "ver: " + Application.version;
        }

        public void StartGame() // Загрузка игровой сцены
        {
            SceneManager.LoadScene(1);
        }

        public void SettingsButton() // Открытие настроек
        {
            StartCoroutine(OpenSettings());
        }

        public void SettingsClosed() // Метод, срабатывающий при закрытии дочерней панели
        {
            StartCoroutine(OpenMenu(false));
        }
        
        public void ExitGame() // Выход из игры
        {
            if (Application.isEditor)
                UnityEditor.EditorApplication.isPlaying = false;
            else
                Application.Quit();
        }

        private void ActivateButtons(bool turnOn)
        {
            foreach (var b in _buttons)
            {
                b.GetComponent<Button>().interactable = turnOn;
            }
        }
        
        // Корутины анимаций
        private IEnumerator OpenMenu(bool withTitle)
        {
            while (_coroutineIsOn) 
            {
                yield return null;
            }

            _coroutineIsOn = true;
            ActivateButtons(false);
            
            yield return new WaitForSeconds(.5f);
            
            _currentTimer = 0f;
            while (_currentTimer < _animationTimer)
            {
                if (withTitle)
                    _title.anchoredPosition = new Vector2(0, Mathf.Lerp(_titleStartYPos, _titleYPos, _currentTimer));
                
                for (int i = 0; i < _buttons.Count; i++)
                {
                    _buttons[i].anchoredPosition =
                        new Vector2(Mathf.Lerp(_buttonStartXPos, 0, _currentTimer), _buttons[i].anchoredPosition.y) * (i % 2 == 0 ? 1 : -1);
                }

                _currentTimer += Time.deltaTime;
                yield return null;
            }

            if (withTitle) _title.anchoredPosition = new Vector2(0, _titleYPos);
            foreach (var button in _buttons)
            {
                button.anchoredPosition = new Vector2(0, button.anchoredPosition.y);
            }

            ActivateButtons(true);
            _coroutineIsOn = false;
        }
        
        private IEnumerator HideMenu()
        {
            while (_coroutineIsOn) 
            {
                yield return null;
            }

            _coroutineIsOn = true;
            ActivateButtons(false);
            yield return new WaitForSeconds(.5f);
            
            _currentTimer = 0f;
            while (_currentTimer < _animationTimer)
            {
                for (int i = 0; i < _buttons.Count; i++)
                {
                    _buttons[i].anchoredPosition =
                        new Vector2(Mathf.Lerp(0, _buttonStartXPos, _currentTimer), _buttons[i].anchoredPosition.y) * (i % 2 == 0 ? 1 : -1);
                }

                _currentTimer += Time.deltaTime;
                yield return null;
            }

            for (int i = 0; i < _buttons.Count; i++)
            {
                _buttons[i].anchoredPosition =
                    i % 2 == 0 ? new Vector2(-_buttonStartXPos, _buttons[i].anchoredPosition.y) : new Vector2(_buttonStartXPos, _buttons[i].anchoredPosition.y);
            }

            ActivateButtons(true);
            _coroutineIsOn = false;
        }

        private IEnumerator OpenSettings()
        {
            StartCoroutine(HideMenu());
            yield return null;

            while (_coroutineIsOn)
            {
                yield return null;
            }

            var panel = Instantiate(_settingsPanel, _canvas.transform);
            panel.GetComponent<Settings>().Init(this);
        }
    }
}
