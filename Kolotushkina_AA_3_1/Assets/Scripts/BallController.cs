﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid
{
    public class BallController : MonoBehaviour
    {
        private bool _movement;
        private float _scale;

        private void Start()
        {
            _scale = transform.localScale.x;
        }

        public void StartMovement()
        {
            transform.SetParent(null);
            _movement = true;
            StartCoroutine(Movement());
        }

        public void ResetTransform()
        {
            transform.localPosition = new Vector3(0, 0, 0.5f);
            transform.localRotation = Quaternion.identity;
        }
        
        private void RecalculateDirection(Vector3 normal) // Перерасчет направления после удара
        {
            var newDirection = Vector3.Reflect(transform.forward, normal);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }

        private void OnCollisionEnter(Collision other)
        {
            RecalculateDirection(other.GetContact(0).normal);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("DeathZone"))
            {
                _movement = false;
                StopCoroutine(Movement());
                GameManager.Instance.DeathZoneCollision();
            }
        }

        public void HideBall()
        {
            _movement = false;
            transform.localScale = Vector3.zero;
        }

        public void ShowBall()
        {
            transform.localScale = Vector3.one * _scale;
        }
        
        private IEnumerator Movement()
        {
            while (_movement)
            {
                transform.position += transform.forward * GameManager.Instance.GetBallSpeed * Time.deltaTime;
                yield return null;
            }
        }
    }
}
