﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Arkanoid
{
    public interface ISettingsManager // Интерфейс, позволяющий фиксировать момент закрытия дочерней панели с UI
    {
        void SettingsClosed();
    }
}
