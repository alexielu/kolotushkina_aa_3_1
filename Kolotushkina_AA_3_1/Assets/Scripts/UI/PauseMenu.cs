﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Arkanoid
{
    public class PauseMenu : MonoBehaviour, ISettingsManager
    {
        [SerializeField] private RectTransform _mainTransform; // Основной родитель, включает в себя фон
        [SerializeField] private RectTransform _childTransform; // "Дочерний" родитель, содержит только элементы интерфеса (кнопки)
        [SerializeField] private List<RectTransform> _buttons;
        [SerializeField] private Settings _settingsPanel;
        
        [Header("Animation Settings")]
        [SerializeField] private float _animationTimer; // Длительность анимации
        private float _currentTimer;
        private bool _coroutineIsOn = false;

        private void OnEnable()
        {
            StartCoroutine(OpenMenu(_mainTransform)); // Анимация появления панели вместе с фоном
        }
        
        public void ResumeLevel() // Продолжение игры
        {
            UIManager.Instance.CloseAllPauseMenu();
        }

        public void Deactivate() // Выключение панели
        {
            StartCoroutine(CloseMenu(_mainTransform,true));
        }

        public void RestartLevel() // Повторный запуск сцены
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void ShowSettings() // Открытие панели с настройками
        {
            StartCoroutine(OpenSettings());
        }

        public void SettingsClosed() // Метод, срабатывающий при закрытии дочерней панели
        {
            StartCoroutine(OpenMenu(_childTransform));
        }

        public void ExitGame() // Выход из игры
        {
            if (Application.isEditor)
                UnityEditor.EditorApplication.isPlaying = false;
            else
                Application.Quit();
        }
        
        private void ActivateButtons(bool turnOn) // Включение / выключение кнопок
        {
            foreach (var b in _buttons)
            {
                b.GetComponent<Button>().interactable = turnOn;
            }
        }

        private IEnumerator OpenMenu(RectTransform transform) // Анимация появления
        {
            while (_coroutineIsOn) // проверка, не дающая анимациям наложиться друг на друга
            {
                yield return null;
            }

            transform.localScale = Vector3.zero;
            ActivateButtons(false);
            _coroutineIsOn = true;
            
            _currentTimer = 0f;
            while (_currentTimer < _animationTimer)
            {
                transform.localScale = Vector3.one * Mathf.Lerp(0, 1, _currentTimer);
                _currentTimer += Time.unscaledDeltaTime;
                yield return null;
            }
            
            transform.localScale = Vector3.one;
            ActivateButtons(true);
            _coroutineIsOn = false;
        }
        
        private IEnumerator CloseMenu(RectTransform transform, bool withDestroy) // Анимация закрытия
        {
            while (_coroutineIsOn)
            {
                yield return null;
            }

            ActivateButtons(false);
            _coroutineIsOn = true;
            _currentTimer = 0f;
            yield return new WaitForSecondsRealtime(.15f);

            while (_currentTimer < _animationTimer)
            {
                transform.localScale = Vector3.one * Mathf.Lerp(1, 0, _currentTimer);
                _currentTimer += Time.unscaledDeltaTime;
                yield return null;
            }

            transform.localScale = Vector3.zero;
            _coroutineIsOn = false;
            ActivateButtons(true);

            if (withDestroy) // Полное закрытие панели, а не временное как при открытии панели настроек
            {
                if (Time.timeScale < 1) 
                    Time.timeScale = 1f;
                
                yield return new WaitForSecondsRealtime(.25f);
                Destroy(gameObject);
            }
        }

        private IEnumerator OpenSettings() // Открытие настроек
        {
            StartCoroutine(CloseMenu(_childTransform,false));
            yield return null;
            
            while (_coroutineIsOn)
            {
                yield return null;
            }
            
            var panel = Instantiate(_settingsPanel, _mainTransform);
            panel.GetComponent<Settings>().Init(this);
        }
    }
}
