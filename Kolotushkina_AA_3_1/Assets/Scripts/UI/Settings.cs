﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Arkanoid
{
    public class Settings : MonoBehaviour
    {
        // Параметры настроек
        [SerializeField] private Toggle _soundToggle;
        [SerializeField] private Slider _volumeSlider;
        [SerializeField] private TMP_Dropdown _difficultyDrop;

        [SerializeField] private RectTransform _panelTransform;
        [SerializeField] private float _animationTimer;
        private float _currentTimer;

        private ISettingsManager _settingsManager; // Скрипт, запустивший окно настроек

        public void Init(ISettingsManager currentSettingsManager)
        {
            _settingsManager = currentSettingsManager;
            LoadParameters();
            StartCoroutine(OpenPanel());
        }

        private void LoadParameters() // Загрузка ранее сохраненных параметров
        {
            _soundToggle.isOn = PlayerPrefs.GetInt("SoundOn") != 0;
            _volumeSlider.value = PlayerPrefs.GetFloat("Volume");
            _difficultyDrop.value = PlayerPrefs.GetInt("Difficulty");
        }

        private void SaveParameters() // Сохранение параметров
        {
            PlayerPrefs.SetInt("SoundOn", _soundToggle.isOn ? 1 : 0);
            PlayerPrefs.SetFloat("Volume", _volumeSlider.value);
            PlayerPrefs.SetInt("Difficulty", _difficultyDrop.value);
        }

        // Методы, выводящие изменения параметров
        public void SoundOnChanged()
        {
            Debug.Log("Sound On: " + _soundToggle.isOn);
        }

        public void VolumeChanged()
        {
            Debug.Log("Volume: " + _volumeSlider.value);
        }

        public void DifficultyChanged()
        {
            Debug.Log("Difficulty: " + _difficultyDrop.options[_difficultyDrop.value].text);
        }

        public void Back() // Выход из окна настроек
        {
            SaveParameters();
            StartCoroutine(ClosePanel());
        }

        // Анимации открытия и закрытия
        private IEnumerator OpenPanel()
        {
            _panelTransform.localScale = Vector3.zero;
            yield return new WaitForSecondsRealtime(.25f);

            _currentTimer = 0;
            while (_currentTimer < _animationTimer)
            {
                _panelTransform.localScale = Vector3.one * Mathf.Lerp(0, 1, _currentTimer);
                _currentTimer += Time.unscaledDeltaTime;
                yield return null;
            }

            _panelTransform.localScale = Vector3.one;
        }

        private IEnumerator ClosePanel()
        {
            _panelTransform.localScale = Vector3.one;
            yield return new WaitForSecondsRealtime(.25f);

            _currentTimer = 0;
            while (_currentTimer < _animationTimer)
            {
                _panelTransform.localScale = Vector3.one * Mathf.Lerp(1, 0, _currentTimer);
                _currentTimer += Time.unscaledDeltaTime;
                yield return null;
            }

            _panelTransform.localScale = Vector3.zero;
            _settingsManager.SettingsClosed();
            Destroy(gameObject);
        }
    }
}
