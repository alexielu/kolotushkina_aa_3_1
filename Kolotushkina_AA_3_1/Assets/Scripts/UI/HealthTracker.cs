﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Arkanoid
{
    public class HealthTracker : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _healthText;

        public void UpdateHealth(int hp) // Обновление кол-ва жизней
        {
            _healthText.text = hp.ToString();
        }
    }
}
