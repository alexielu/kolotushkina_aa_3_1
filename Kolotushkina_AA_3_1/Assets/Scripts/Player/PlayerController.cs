﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

namespace Arkanoid
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour
    {
        protected PlayerControls _controls;
        protected InputAction _moveAction;
        private Coroutine _movement;
        private Rigidbody _rb;
        
        private void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _controls = new PlayerControls();
            _controls.Enable();
            InitMovement();
        }

        protected virtual void InitMovement() // Фиксирование нужного Input Action'а
        {
            _moveAction = _controls.PlayerBehaviour.MoveArrows;
        }
        
        private void Update()
        {
            if (GameManager.Instance.InputFreezed) return;
            
            var value = _moveAction.ReadValue<Vector2>();
            var direction = transform.right * value.x + transform.up * value.y;
            _rb.AddForce(GameManager.Instance.GetSpeedMultiplier * direction.normalized, ForceMode.Impulse);
        }
        
        protected void OnDisable()
        {
            _controls.Disable();
        }

        private void OnDestroy()
        {
            _controls.Dispose();
        }
    }
}

