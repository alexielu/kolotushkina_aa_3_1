﻿using System.Collections;
using System.Collections.Generic;
using Arkanoid;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

namespace Arkanoid
{
    public class FirstPlayerController : PlayerController
    {
        private bool _ballIsHere;

        protected override void InitMovement()
        {
            _moveAction = _controls.PlayerBehaviour.MoveWASD;
            _controls.PlayerBehaviour.PushBall.performed += OnBallPush;
            _controls.Interface.PauseMenu.performed += OnPause;
        }

        public void SetBall() // Фиксирование шара перед игроком
        {
            var ball = GameManager.Instance.GetBall;
            ball.transform.SetParent(transform);
            ball.ResetTransform();
            _ballIsHere = true;
        }

        private void OnBallPush(CallbackContext context)
        {
            if (!_ballIsHere || GameManager.Instance.InputFreezed) return;

            _ballIsHere = false;
            GameManager.Instance.GetBall.StartMovement();
        }

        private void OnPause(CallbackContext context) // Вызов меню паузы
        {
            UIManager.Instance.Pause();
        }
    }
}
